# PBServices

Phone Book Project

## Başlangıç

Birbirleri ile haberleşen iki microservice'in olduğu basit bir telefon rehberine ait BE yapısı tasarlanmaya çalışılmıştır. 


## Dosyalar

cd existing_repo <br/>
git remote add origin https://gitlab.com/phonebook580/pbservices.git <br/>
git branch -M main <br/>
git push -uf origin main <br/>


## İşlevler

• Rehberde kişi oluşturma <br/>
• Rehberde kişi kaldırma  <br/>
• Rehberdeki kişiye iletişim bilgisi ekleme <br/>
• Rehberdeki kişiden iletişim bilgisi kaldırma <br/>
• Rehberdeki kişilerin listelenmesi <br/>
• Rehberdeki bir kişiyle ilgili iletişim bilgilerinin de yer aldığı detay bilgilerin 
getirilmesi <br/>
• Rehberdeki kişilerin bulundukları konuma göre istatistiklerini çıkartan bir rapor 
talebi <br/>
• Sistemin oluşturduğu raporların listelenmesi <br/>
• Sistemin oluşturduğu bir raporun detay bilgilerinin getirilmesi <br/>


## Teknik Tasarım

Kişiler: Sistemde teorik anlamda sınırsız sayıda kişi kaydı yapılabilecektir. Her kişiye 
bağlı iletişim bilgileri de yine sınırsız bir biçimde eklenebilmelidir.
Karşılanması beklenen veri yapısındaki gerekli alanlar aşağıdaki gibidir:
• int <br/>
• Ad <br/>
• Soyad <br/>
• Firma <br/>
• İletişim Bilgisi <br/>
o Bilgi Tipi: Telefon Numarası, E-mail Adresi, Konum <br/>
o Bilgi İçeriği <br/>
Rapor: Rapor talepleri asenkron çalışacaktır. Kullanıcı bir rapor talep ettiğinde, sistem 
arkaplanda bu çalışmayı darboğaz yaratmadan sıralı bir biçimde ele alacak; rapor 
tamamlandığında ise kullanıcının "raporların listelendiği" endpoint üzerinden raporun 
durumunu "tamamlandı" olarak gözlemleyebilmesi gerekmektedir. <br/>
Rapor basitçe aşağıdaki bilgileri içerecektir: <br/>
• Konum Bilgisi <br/>
• O konumda yer alan rehbere kayıtlı kişi sayısı <br/>
• O konumda yer alan rehbere kayıtlı telefon numarası sayısı 
Veri yapısı olarak da: <br/>
• int <br/>
• Raporun Talep Edildiği Tarih <br/>
• Rapor Durumu (Hazırlanıyor, Tamamlandı) <br/>


# Kullanılan Teknolojiler <br/>

o .NET Core<br/>
o GitLab<br/> o Postgres<br/>
o Rabbit MQ<br/>

## Architecture
![Assessment_Example_Architecture.png](https://gitlab.com/phonebook580/pbservices/-/raw/52e43c7c41bd3163fa3d6050bb4a525e9d1a965c/Assessment_Example_Architecture.png)


